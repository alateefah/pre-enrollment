/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.manager;

import com.edo.enrol.pojo.AppPreEnrolDetail;
import com.edo.enrol.util.exception.GeneralAppException;

/**
 *
 * @author Lateefah
 */
public interface PreEnrolDetailManagerLocal {
    
    AppPreEnrolDetail addEmployee(String employeeId, String firstName, String surname, String middleName,
            String dateOfBirth, String personType, String staffType, String mda, String dateOfEmployment,
            String salaryStructure, String gradeLevel, String step, String rawToken) throws GeneralAppException;
    
}
