/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.manager;


import com.edo.enrol.data.manager.MdaLookupDataManagerLocal;
import com.edo.enrol.model.Mda;
import com.edo.enrol.pojo.AppMdaLookup;
import com.edo.enrol.util.Verifier;
import com.edo.enrol.util.exception.GeneralAppException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */

@Stateless
public class MdaLookupManager implements MdaLookupManagerLocal {
    
    @EJB
    private MdaLookupDataManagerLocal mdaLookupDataManager;
            
    @EJB
    private Verifier verifier;
    
    private final String MDA_LINK = "/lookup/mda";
        
    @Override
    public List<AppMdaLookup> getAll (String rawToken)  throws GeneralAppException {        
        
        verifier.setResourceUrl(MDA_LINK).verifyParams(rawToken);
        verifier.setResourceUrl(MDA_LINK).verifyJwt(rawToken);
        
        List<Mda> mdas = mdaLookupDataManager.getAll();
        List<AppMdaLookup> appMdas = new ArrayList<AppMdaLookup>();
        for (Mda mda: mdas) {
            appMdas.add(getMdaLookup(mda));
        }
        
        return appMdas; 
    }
    
       
    public AppMdaLookup getMdaLookup (Mda mda) {
        AppMdaLookup appMdaLookup = new AppMdaLookup();
        appMdaLookup.setMdaId(mda.getMdaId());
        appMdaLookup.setDescription(mda.getDescription());
        return appMdaLookup;
    }

}
