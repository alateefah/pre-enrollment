/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.manager;

import com.edo.enrol.data.manager.ExceptionThrowerManagerLocal;
import com.edo.enrol.data.manager.PersonTypeLookupDataManagerLocal;
import com.edo.enrol.data.manager.UserDataManagerLocal;
import com.edo.enrol.model.PersonType;
import com.edo.enrol.model.User;
import com.edo.enrol.pojo.AppPersonTypeLookup;
import com.edo.enrol.pojo.AppUser;
import com.edo.enrol.util.JWT;
import com.edo.enrol.util.MD5;
import com.edo.enrol.util.Verifier;
import com.edo.enrol.util.exception.GeneralAppException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */

@Stateless
public class PersonTypeLookupManager implements PersonTypeLookupManagerLocal {
    
    @EJB
    private PersonTypeLookupDataManagerLocal personTypeLookupDataManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;
        
    @EJB
    private Verifier verifier;
    
    private final String PERSON_TYPE_LINK = "/lookup/person-type";
        
    @Override
    public List<AppPersonTypeLookup> getAll (String rawToken)  throws GeneralAppException {        
        
        verifier.setResourceUrl(PERSON_TYPE_LINK).verifyParams(rawToken);
        verifier.setResourceUrl(PERSON_TYPE_LINK).verifyJwt(rawToken);
        
        List<PersonType> personTypes = personTypeLookupDataManager.getAll();
        List<AppPersonTypeLookup> appPersonTypes = new ArrayList<AppPersonTypeLookup>();
        for (PersonType personType: personTypes) {
            appPersonTypes.add(getPersonTypeLookup(personType));
        }
        
        return appPersonTypes; 
    }
    
       
    public AppPersonTypeLookup getPersonTypeLookup (PersonType personType) {
        AppPersonTypeLookup appPersonTypeLookup = new AppPersonTypeLookup();
        appPersonTypeLookup.setPersonTypeId(personType.getPersonTypeId());
        appPersonTypeLookup.setDescription(personType.getDescription());
        return appPersonTypeLookup;
    }

}
