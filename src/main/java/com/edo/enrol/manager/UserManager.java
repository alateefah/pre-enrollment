/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.manager;

import com.edo.enrol.data.manager.BooleanManagerLocal;
import com.edo.enrol.data.manager.ExceptionThrowerManagerLocal;
import com.edo.enrol.data.manager.UserDataManagerLocal;
import com.edo.enrol.model.User;
import com.edo.enrol.pojo.AppBoolean;
import com.edo.enrol.pojo.AppUser;
import com.edo.enrol.util.JWT;
import com.edo.enrol.util.MD5;
import com.edo.enrol.util.Verifier;
import com.edo.enrol.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */

@Stateless
public class UserManager implements UserManagerLocal {
    
    @EJB
    private UserDataManagerLocal userDataManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;
    
    @EJB
    private BooleanManagerLocal booleanManager;
        
    @EJB
    private Verifier verifier;
    
    private final String USER_LINK = "/user";
      
    @Override
    public AppUser addUser(String username, String password, String firstname, 
            String othernames) throws GeneralAppException {    
        
        verifier.setResourceUrl(USER_LINK).verifyParams(username, password, firstname, othernames);
                
        User user = new User();
            
        user.setUsername(username);
        user.setFirstName(firstname);
        user.setOtherNames(othernames);
        user.setPassword(MD5.hash(password));

        userDataManager.create(user);
            
        return getAppUser(user);
    }
     
     
    @Override
    public User getUser(String username) {
        return userDataManager.get(username);
    }
    
    @Override
    public AppUser getAppUser(User user) {
        AppUser appUser = new AppUser();
        if (user != null) {
            appUser.setUsername(user.getUsername());
            appUser.setFirstName(user.getFirstName());
            appUser.setOtherNames(user.getOtherNames());            
        }
        
        return appUser;
    }
    
    @Override
    public AppBoolean changeUserPassword(String username, String oldPassword, String newPassword) 
            throws GeneralAppException{
        
        verifier.setResourceUrl(USER_LINK).verifyParams(username, oldPassword, newPassword);
        User user = getUser(username);
        
        if (user == null) {
            exceptionManager.throwUserDoesNotExistException(USER_LINK);
        }
        
        if (!user.getPassword().equals(MD5.hash(oldPassword))) {
             exceptionManager.throwInvalidOldPasswordException(USER_LINK);
        }
        
        user.setPassword(newPassword);
        
        userDataManager.update(user);
        
        return booleanManager.returnBoolean(true);
    }

}
