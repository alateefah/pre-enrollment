/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.manager;

import com.edo.enrol.pojo.AppBoolean;
import com.edo.enrol.pojo.AppUser;
import com.edo.enrol.util.exception.GeneralAppException;

/**
 *
 * @author Lateefah
 */
public interface UserLoginManagerLocal {
    
    AppUser authenticateUser(String username, String password) throws GeneralAppException;
    
    AppBoolean logoutUser(String username, String rawToken) throws GeneralAppException;
    
    
}
