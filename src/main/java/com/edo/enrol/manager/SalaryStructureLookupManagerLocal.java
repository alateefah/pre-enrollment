/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.manager;

import com.edo.enrol.pojo.AppSalaryStructureLookup;
import com.edo.enrol.util.exception.GeneralAppException;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public interface SalaryStructureLookupManagerLocal {
    
    List<AppSalaryStructureLookup> getAll(String rawToken) throws GeneralAppException;

}
