/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.manager;


import com.edo.enrol.data.manager.GlLookupDataManagerLocal;
import com.edo.enrol.model.Gl;
import com.edo.enrol.pojo.AppGlLookup;
import com.edo.enrol.util.Verifier;
import com.edo.enrol.util.exception.GeneralAppException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */

@Stateless
public class GlLookupManager implements GlLookupManagerLocal {
    
    @EJB
    private GlLookupDataManagerLocal glLookupDataManager;
            
    @EJB
    private Verifier verifier;
    
    private final String GL_LINK = "/lookup/gl";
        
    @Override
    public List<AppGlLookup> getAll (String rawToken)  throws GeneralAppException {        
        
        verifier.setResourceUrl(GL_LINK).verifyParams(rawToken);
        verifier.setResourceUrl(GL_LINK).verifyJwt(rawToken);
        
        List<Gl> gls = glLookupDataManager.getAll();
        List<AppGlLookup> appGls = new ArrayList<AppGlLookup>();
        for (Gl gl: gls) {
            appGls.add(getGlLookup(gl));
        }
        
        return appGls; 
    }
    
       
    public AppGlLookup getGlLookup (Gl gl) {
        AppGlLookup appGlLookup = new AppGlLookup();
        appGlLookup.setGlId(gl.getGlId());
        appGlLookup.setDescription(gl.getDescription());
        return appGlLookup;
    }

}
