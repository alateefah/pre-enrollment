/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.manager;


import com.edo.enrol.data.manager.SalaryStructureLookupDataManagerLocal;
import com.edo.enrol.model.SalaryStructure;
import com.edo.enrol.pojo.AppSalaryStructureLookup;
import com.edo.enrol.util.Verifier;
import com.edo.enrol.util.exception.GeneralAppException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */

@Stateless
public class SalaryStructureLookupManager implements SalaryStructureLookupManagerLocal {
    
    @EJB
    private SalaryStructureLookupDataManagerLocal salaryStructureLookupDataManager;
            
    @EJB
    private Verifier verifier;
    
    private final String GL_LINK = "/lookup/salaryStructure";
        
    @Override
    public List<AppSalaryStructureLookup> getAll (String rawToken)  throws GeneralAppException {        
        
        verifier.setResourceUrl(GL_LINK).verifyParams(rawToken);
        verifier.setResourceUrl(GL_LINK).verifyJwt(rawToken);
        
        List<SalaryStructure> salaryStructures = salaryStructureLookupDataManager.getAll();
        List<AppSalaryStructureLookup> appSalaryStructures = new ArrayList<AppSalaryStructureLookup>();
        for (SalaryStructure salaryStructure: salaryStructures) {
            appSalaryStructures.add(getSalaryStructureLookup(salaryStructure));
        }
        
        return appSalaryStructures; 
    }
    
       
    public AppSalaryStructureLookup getSalaryStructureLookup (SalaryStructure salaryStructure) {
        AppSalaryStructureLookup appSalaryStructureLookup = new AppSalaryStructureLookup();
        appSalaryStructureLookup.setSalaryStructureId(salaryStructure.getSalaryStructureId());
        appSalaryStructureLookup.setDescription(salaryStructure.getDescription());
        return appSalaryStructureLookup;
    }

}
