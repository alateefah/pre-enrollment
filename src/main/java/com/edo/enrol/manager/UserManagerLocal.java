/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.manager;

import com.edo.enrol.model.User;
import com.edo.enrol.pojo.AppBoolean;
import com.edo.enrol.pojo.AppUser;
import com.edo.enrol.util.exception.GeneralAppException;

/**
 *
 * @author Lateefah
 */
public interface UserManagerLocal {
    
    AppUser addUser(String username, String password, String firstname, String othernames)
            throws GeneralAppException;
        
    AppUser getAppUser(User user);
    
    User getUser(String username);
    
    AppBoolean changeUserPassword(String username, String oldPassword, String newPassword)
            throws GeneralAppException;;
}
