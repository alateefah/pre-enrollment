/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.manager;

import com.edo.enrol.data.manager.ExceptionThrowerManagerLocal;
import com.edo.enrol.data.manager.PreEnrolDetailDataManagerLocal;
import com.edo.enrol.model.PreEnrolDetail;
import com.edo.enrol.pojo.AppPreEnrolDetail;
import com.edo.enrol.util.CodeGenerator;
import com.edo.enrol.util.Verifier;
import com.edo.enrol.util.exception.GeneralAppException;
import java.sql.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */

@Stateless
public class PreEnrolDetailManager implements PreEnrolDetailManagerLocal {
    
    @EJB
    private PreEnrolDetailDataManagerLocal preEnrolDetailDataManagerLocal;    
    
    @EJB
    private CodeGenerator codeGenerator;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;
        
    @EJB
    private Verifier verifier;
    
    private final String PRE_ENROL_DETAIL_LINK = "/pre-enrol";
        

    @Override
    public AppPreEnrolDetail addEmployee(String employeeId, String firstName, String surname, String middleName,
            String dateOfBirth, String personType, String staffType, String mda, String dateOfEmployment,
            String salaryStructure, String gradeLevel, String step, String rawToken) throws GeneralAppException {    
        
        verifier.setResourceUrl(PRE_ENROL_DETAIL_LINK).verifyParams(employeeId, firstName, surname, 
                dateOfBirth, personType, staffType, mda, dateOfEmployment, salaryStructure, gradeLevel, step, rawToken);
           
        verifier.setResourceUrl(PRE_ENROL_DETAIL_LINK).verifyInteger(personType, staffType, mda, gradeLevel, step);
        
        verifier.setResourceUrl(PRE_ENROL_DETAIL_LINK).verifyDate(dateOfBirth, dateOfEmployment);
                
        verifier.setResourceUrl(PRE_ENROL_DETAIL_LINK).verifyJwt(rawToken);
        
        PreEnrolDetail employeeExist = getEmployee(employeeId);
        if (employeeExist != null) {
            return getAppPreEnrolDetail(employeeExist);
        }
        
        PreEnrolDetail preEnrolDetail = new PreEnrolDetail();
          
        preEnrolDetail.setEmployeeId(employeeId);
        preEnrolDetail.setFirstName(firstName);
        preEnrolDetail.setMiddleName(middleName);
        preEnrolDetail.setSurname(surname);
        preEnrolDetail.setDateOfBirth(Date.valueOf(dateOfBirth));
        preEnrolDetail.setPersonType(Integer.parseInt(personType));
        preEnrolDetail.setStaffType(Integer.parseInt(staffType));
        preEnrolDetail.setMda(Integer.parseInt(mda));
        preEnrolDetail.setDateOfEmployment(Date.valueOf(dateOfEmployment));
        preEnrolDetail.setSalaryStructure(Integer.parseInt(salaryStructure));
        preEnrolDetail.setGradeLevel(Integer.parseInt(gradeLevel));
        preEnrolDetail.setStep(Integer.parseInt(step));
        preEnrolDetail.setGeneratedFormId(codeGenerator.getGeneratedFormId(employeeId));
        
        preEnrolDetailDataManagerLocal.create(preEnrolDetail);
            
        return getAppPreEnrolDetail(preEnrolDetail);
    }
     
   
    private PreEnrolDetail getEmployee (String employeeId) {
        return preEnrolDetailDataManagerLocal.get(employeeId);
    }
    
    private AppPreEnrolDetail getAppPreEnrolDetail(PreEnrolDetail preEnrolDetail) {
        AppPreEnrolDetail appPreEnrolDetail = new AppPreEnrolDetail();
 
        appPreEnrolDetail.setGeneratedFormId(preEnrolDetail.getGeneratedFormId());
        appPreEnrolDetail.setEmployeeId(preEnrolDetail.getEmployeeId());
        appPreEnrolDetail.setFirstName(preEnrolDetail.getFirstName());
        appPreEnrolDetail.setMiddleName(preEnrolDetail.getMiddleName());
        appPreEnrolDetail.setSurname(preEnrolDetail.getSurname());
        appPreEnrolDetail.setDateOfBirth(preEnrolDetail.getDateOfBirth());
        appPreEnrolDetail.setPersonType(preEnrolDetail.getPersonType());
        appPreEnrolDetail.setMda(preEnrolDetail.getMda());
        appPreEnrolDetail.setDateOfEmployment(preEnrolDetail.getDateOfEmployment());
        appPreEnrolDetail.setSalaryStructure(preEnrolDetail.getSalaryStructure());        
        appPreEnrolDetail.setGradeLevel(preEnrolDetail.getGradeLevel());
        appPreEnrolDetail.setStep(preEnrolDetail.getStep());
               
        return appPreEnrolDetail;
    }

}
