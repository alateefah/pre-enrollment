/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.manager;


import com.edo.enrol.data.manager.StaffTypeLookupDataManagerLocal;
import com.edo.enrol.model.StaffType;
import com.edo.enrol.pojo.AppStaffTypeLookup;
import com.edo.enrol.pojo.AppStaffTypeLookup;
import com.edo.enrol.util.Verifier;
import com.edo.enrol.util.exception.GeneralAppException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */

@Stateless
public class StaffTypeLookupManager implements StaffTypeLookupManagerLocal {
    
    @EJB
    private StaffTypeLookupDataManagerLocal staffTypeLookupDataManager;
    
          
    @EJB
    private Verifier verifier;
    
    private final String PERSON_TYPE_LINK = "/lookup/staff-type";
        
    @Override
    public List<AppStaffTypeLookup> getAll (String rawToken)  throws GeneralAppException {        
        
        verifier.setResourceUrl(PERSON_TYPE_LINK).verifyParams(rawToken);
        verifier.setResourceUrl(PERSON_TYPE_LINK).verifyJwt(rawToken);
        
        List<StaffType> staffTypes = staffTypeLookupDataManager.getAll();
        List<AppStaffTypeLookup> appStaffTypes = new ArrayList<AppStaffTypeLookup>();
        for (StaffType staffType: staffTypes) {
            appStaffTypes.add(getStaffTypeLookup(staffType));
        }
        
        return appStaffTypes; 
    }
    
       
    public AppStaffTypeLookup getStaffTypeLookup (StaffType staffType) {
        AppStaffTypeLookup appStaffTypeLookup = new AppStaffTypeLookup();
        appStaffTypeLookup.setStaffTypeId(staffType.getStaffTypeId());
        appStaffTypeLookup.setDescription(staffType.getDescription());
        return appStaffTypeLookup;
    }

}
