/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.manager;


import com.edo.enrol.data.manager.StepLookupDataManagerLocal;
import com.edo.enrol.model.Step;
import com.edo.enrol.pojo.AppStepLookup;
import com.edo.enrol.util.Verifier;
import com.edo.enrol.util.exception.GeneralAppException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */

@Stateless
public class StepLookupManager implements StepLookupManagerLocal {
    
    @EJB
    private StepLookupDataManagerLocal StepLookupDataManager;
            
    @EJB
    private Verifier verifier;
    
    private final String Step_LINK = "/lookup/Step";
        
    @Override
    public List<AppStepLookup> getAll (String rawToken)  throws GeneralAppException {        
        
        verifier.setResourceUrl(Step_LINK).verifyParams(rawToken);
        verifier.setResourceUrl(Step_LINK).verifyJwt(rawToken);
        
        List<Step> Steps = StepLookupDataManager.getAll();
        List<AppStepLookup> appSteps = new ArrayList<AppStepLookup>();
        for (Step Step: Steps) {
            appSteps.add(getStepLookup(Step));
        }
        
        return appSteps; 
    }
    
       
    public AppStepLookup getStepLookup (Step Step) {
        AppStepLookup appStepLookup = new AppStepLookup();
        appStepLookup.setStepId(Step.getStepId());
        appStepLookup.setDescription(Step.getDescription());
        return appStepLookup;
    }

}
