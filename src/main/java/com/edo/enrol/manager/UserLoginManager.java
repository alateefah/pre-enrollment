/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.manager;


import com.edo.enrol.data.manager.BooleanManagerLocal;
import com.edo.enrol.data.manager.ExceptionThrowerManagerLocal;
import com.edo.enrol.data.manager.UserLoginDataManagerLocal;
import com.edo.enrol.model.User;
import com.edo.enrol.model.UserLogin;
import com.edo.enrol.pojo.AppBoolean;
import com.edo.enrol.pojo.AppUser;
import com.edo.enrol.util.JWT;
import com.edo.enrol.util.MD5;
import com.edo.enrol.util.Verifier;
import com.edo.enrol.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Lateefah
 */

@Stateless
public class UserLoginManager implements UserLoginManagerLocal {
    
    @EJB
    private UserManagerLocal userManager;
    
    @EJB
    private UserLoginDataManagerLocal userLoginDataManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;

    @EJB
    private BooleanManagerLocal booleanManager;
    
    @EJB
    private Verifier verifier;
    
    private final String AUTHENTICATE_USER_LINK = "/user/authenticate";
    private final String LOGOUT_USER_LINK = "/user/logout";
    private final long TOKEN_LIFETIME= 31556952000l;
        
    @Override
    public AppUser authenticateUser (String username, String password)
            throws GeneralAppException {        
        
        verifier.setResourceUrl(AUTHENTICATE_USER_LINK).verifyParams(username, password);
        
        User user = userManager.getUser(username);
        
        if (user == null) {
            exceptionManager.throwUserDoesNotExistException(AUTHENTICATE_USER_LINK);
        }
        
        if (!user.getPassword().equals(MD5.hash(password))) {            
            exceptionManager.throwInvalidLoginCredentialsException(AUTHENTICATE_USER_LINK);
        } 
        
        UserLogin userLogin = new UserLogin();
        userLogin.setUsername(username);       
        userLogin.setLoginTime(new Timestamp(System.currentTimeMillis()));
        userLogin.setLogoutTime(null);
        
        userLoginDataManager.create(userLogin);
        
        return getAppUserWithToken(getAppUser(user)); 
    }
    
    @Override
    public AppBoolean logoutUser (String username, String rawToken)
            throws GeneralAppException {
        
        verifier.setResourceUrl(LOGOUT_USER_LINK).verifyParams(username, rawToken);
        verifier.setResourceUrl(LOGOUT_USER_LINK).verifyJwt(rawToken);
        
        List<UserLogin> lastUserLogin = userLoginDataManager.getLastUserLogin(username);
        if (!lastUserLogin.isEmpty()) {           
            lastUserLogin.get(0).setLogoutTime(new Timestamp(System.currentTimeMillis()));
            userLoginDataManager.update(lastUserLogin.get(0));
        }
        
        //TODO destroy token
        return booleanManager.returnBoolean(true);
    }
    private AppUser getAppUser(User user) {
        return userManager.getAppUser(user);
    }
         
    private AppUser getAppUserWithToken(AppUser appUser) {
        JWT token = new JWT();      
        appUser.setAuthToken(token.createJWT(appUser, TOKEN_LIFETIME));
        return appUser;
    }
}
