/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.manager;

import com.edo.enrol.pojo.AppMdaLookup;
import com.edo.enrol.util.exception.GeneralAppException;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public interface MdaLookupManagerLocal {
    
    List<AppMdaLookup> getAll(String rawToken) throws GeneralAppException;

}
