/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.util;

import java.util.Random;
import javax.ejb.Stateless;
import java.security.SecureRandom;
import java.math.BigInteger;

/**
 *
 * @author buls
 */
@Stateless
public class CodeGenerator {
    
    public String getCode() {
        Integer randomCode = new Random().nextInt((99999 - 10000) + 1) + 10000;
        return randomCode.toString();
    }
    

    public String getToken() {
        SecureRandom random = new SecureRandom();    
        return new BigInteger(130, random).toString(32);
    }
    
    public String getShortToken() {
        String token = getToken();
        token = token.substring(token.length()/2, token.length());
        return token;
    }
    
    public String getGeneratedFormId(String employeeId) {        
        //String employeeShort = employeeId.substring(employeeId.length()-6, employeeId.length());
        String shortToken1 = getCode();
        String shortToken2 = getCode();
        String generatedFormId = employeeId + "-" + shortToken1 + "-" + shortToken2;
        return generatedFormId;
    }
    
}