/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.data.manager;

import com.edo.enrol.model.User;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Lateefah
 */
@Local
public interface UserDataManagerLocal {
    
    User create(User user);

    User update(User user);

    User get(String userId);

    void delete(User user);

    List<User> getAllUsers();
}
