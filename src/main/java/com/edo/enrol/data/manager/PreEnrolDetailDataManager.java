/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.data.manager;

import com.edo.enrol.data.provider.DataProviderLocal;
import com.edo.enrol.model.PreEnrolDetail;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class PreEnrolDetailDataManager implements PreEnrolDetailDataManagerLocal {
    @EJB
    private DataProviderLocal crud;    

    @Override
    public PreEnrolDetail create(PreEnrolDetail preEnrolDetail) {
        return crud.create(preEnrolDetail);
    }


    @Override
    public PreEnrolDetail get(String employeeId) {
        return crud.find(employeeId, PreEnrolDetail.class);
    }
  
}
