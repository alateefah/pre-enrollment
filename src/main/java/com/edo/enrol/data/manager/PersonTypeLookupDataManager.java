/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.data.manager;

import com.edo.enrol.data.provider.DataProviderLocal;
import com.edo.enrol.model.PersonType;
import com.edo.enrol.model.PreEnrolDetail;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class PersonTypeLookupDataManager implements PersonTypeLookupDataManagerLocal {
    @EJB
    private DataProviderLocal crud;    

    @Override
    public List<PersonType> getAll() {
        return crud.findAll(PersonType.class);
    }
  
}
