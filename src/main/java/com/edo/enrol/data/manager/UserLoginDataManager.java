/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.data.manager;

import com.edo.enrol.data.provider.DataProviderLocal;
import com.edo.enrol.model.UserLogin;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class UserLoginDataManager implements UserLoginDataManagerLocal {
    @EJB
    private DataProviderLocal crud;    

    @Override
    public UserLogin create(UserLogin userLogin) {
        return crud.create(userLogin);
    }

    @Override
    public UserLogin update(UserLogin userLogin) {
        return crud.update(userLogin);
    }

    @Override
    public UserLogin get(String connectionId) {
        return crud.find(connectionId, UserLogin.class);
    }

    @Override
    public List<UserLogin> getLastUserLogin(String username) {
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("username", username);
        return crud.findByNamedQuery("UserLogin.findUserLastLogin", parameters, UserLogin.class);
    }
     
}
