/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.data.manager;

import com.edo.enrol.pojo.AppBoolean;
import javax.ejb.Stateless;

/**
 *
 * @author buls
 */

@Stateless
public class BooleanManager implements BooleanManagerLocal {
    
    
    @Override
    public AppBoolean returnBoolean (Boolean state) {
        AppBoolean appBoolean = new AppBoolean();
        
        appBoolean.setState(state);
        
        return appBoolean;
    }
}

