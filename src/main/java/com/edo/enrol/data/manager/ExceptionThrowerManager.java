/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.data.manager;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import com.edo.enrol.util.exception.GeneralAppException;

/**
 *
 * @author buls
 */

@Stateless
public class ExceptionThrowerManager implements ExceptionThrowerManagerLocal {
    
       
    private final String INCOMPLETE_DATA_ERROR = "Incomplete data";
    private final String INCOMPLETE_DATA_ERROR_DETAILS = "Incomplete has been provided";

    private final String INVALID_TOKEN_ERROR = "Invalid token";
    private final String INVALID_TOKEN_ERROR_DETAILS = "Invalid or expired token supplied";
    
    private final String INVALID_INTEGER_ERROR = "Invalid integer";
    private final String INVALID_INTEGER_ERROR_DETAILS = "Invalid integer supplied";
    
    private final String INVALID_DATE_ERROR = "Invalid date";
    private final String INVALID_DATE_ERROR_DETAILS = "Invalid date supplied. Date should be in format yyyy-mm-dd";
    
    private final String USER_NOT_FOUND_ERROR = "User not found";
    private final String USER_NOT_FOUND_ERROR_DETAILS = "The user with the supplied details id does not exist";
    
    private final String INVALID_LOGIN_CREDENTIALS_ERROR = "Invalid login credentials";
    private final String INVALID_LOGIN_CREDENTIALS_ERROR_DETAILS = "Invalid Username/Password combination";     
    
    private final String EMPLOYEE_ALREADY_EXIST_ERROR = "Employee record exists";
    private final String EMPLOYEE_ALREADY_EXIST_ERROR_DETAILS = "Employee record exists";
    
    private final String INVALID_OLD_PASSWORD_ERROR= "Invalid Old password";
    private final String INVALID_OLD_PASSWORD_ERROR_DETAILS= "The old passsword provided is wrong";
    
    @Override
    public void throwNullReviewAttributesException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, INCOMPLETE_DATA_ERROR, INCOMPLETE_DATA_ERROR_DETAILS, link);
    }

    @Override
    public void throwInvalidTokenException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, INVALID_TOKEN_ERROR, INVALID_TOKEN_ERROR_DETAILS, link);
    }
    
    @Override
    public void throwInvalidIntegerAttributeException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, INVALID_INTEGER_ERROR, INVALID_INTEGER_ERROR_DETAILS, link);
    }   
    
    @Override
    public void throwInvalidDateAttributeException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, INVALID_DATE_ERROR, INVALID_DATE_ERROR_DETAILS, link);
    }   
    
    @Override
    public void throwUserDoesNotExistException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, USER_NOT_FOUND_ERROR, USER_NOT_FOUND_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidLoginCredentialsException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_LOGIN_CREDENTIALS_ERROR, INVALID_LOGIN_CREDENTIALS_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwEmployeeAlreadyExistException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, EMPLOYEE_ALREADY_EXIST_ERROR, EMPLOYEE_ALREADY_EXIST_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidOldPasswordException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_OLD_PASSWORD_ERROR, INVALID_OLD_PASSWORD_ERROR_DETAILS,
                link);
    }
}
