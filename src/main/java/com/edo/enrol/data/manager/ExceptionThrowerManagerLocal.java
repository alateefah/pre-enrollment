/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.data.manager;

import com.edo.enrol.util.exception.GeneralAppException;
/**
 *
 * @author Lateefah
 */
public interface ExceptionThrowerManagerLocal {
   
    void throwNullReviewAttributesException(String link) throws GeneralAppException;
    
    void throwInvalidTokenException(String link) throws GeneralAppException;
    
    void throwInvalidIntegerAttributeException(String link) throws GeneralAppException; 
    
    void throwInvalidDateAttributeException(String link) throws GeneralAppException;
    
    void throwUserDoesNotExistException(String link) throws GeneralAppException;
    
    void throwInvalidLoginCredentialsException(String link) throws GeneralAppException;
    
    void throwEmployeeAlreadyExistException(String link) throws GeneralAppException;
    
    void throwInvalidOldPasswordException(String link) throws GeneralAppException;
}
