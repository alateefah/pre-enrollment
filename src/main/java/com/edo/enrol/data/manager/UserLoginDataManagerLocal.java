/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.data.manager;

import com.edo.enrol.model.User;
import com.edo.enrol.model.UserLogin;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Lateefah
 */
@Local
public interface UserLoginDataManagerLocal {
    
    UserLogin create(UserLogin userLogin);

    UserLogin update(UserLogin userLogin);

    UserLogin get(String connectionId);

    List<UserLogin> getLastUserLogin(String username);
}
