/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.data.manager;

import com.edo.enrol.model.Gl;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Lateefah
 */
@Local
public interface GlLookupDataManagerLocal {
    
    List<Gl> getAll();
    
}
