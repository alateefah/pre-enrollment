/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.config;

import com.edo.enrol.service.LookupService;
import com.edo.enrol.service.PreEnrolDetailService;
import com.edo.enrol.service.UserService;
import com.edo.enrol.util.exception.GeneralAppExceptionMapper;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Lateefah
 */
@javax.ws.rs.ApplicationPath("/api")

public class ApplicationConfig extends Application{
    @Override    
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        
        // add services here
        s.add(UserService.class);
        s.add(PreEnrolDetailService.class);
        s.add(LookupService.class);
		
        s.add(GeneralAppExceptionMapper.class);
        
        return s;
    }
}
