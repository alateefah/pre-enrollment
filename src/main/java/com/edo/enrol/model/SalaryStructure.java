/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Lateefah
 */

@Entity
@Table(name = "salary_structure")

public class SalaryStructure implements Serializable{
    
    @Id
    @Column(name = "salary_structure_id")
    private int salaryStructureId;
    
    @Column(name = "description")
    private String description;
       
    public SalaryStructure() {}

    public int getSalaryStructureId() {
        return salaryStructureId;
    }

    public void setSalaryStructureId(int salaryStructureId) {
        this.salaryStructureId = salaryStructureId;
    }
  
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
}
