/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Lateefah
 */
@Entity
@Table(name = "user_login")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "UserLogin.findUserLastLogin", query = "SELECT ul FROM UserLogin ul WHERE ul.username = :username and ul.logoutTime IS NULL")
})

public class UserLogin implements Serializable{
    
    @Id
    @Column(name = "connection_id")
    private int connectionId;
    
    @NotNull
    @Column(name = "username")
    private String username;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "login_time")
    private Date loginTime;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "logout_time")
    private Date logoutTime;
    
    public UserLogin() {
        
    }

    public int getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public Date getLogoutTime() {
        return logoutTime;
    }

    public void setLogoutTime(Date logoutTime) {
        this.logoutTime = logoutTime;
    }

    
        
}