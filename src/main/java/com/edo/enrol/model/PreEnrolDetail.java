/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Lateefah
 */

@Entity
@Table(name = "pre_enrol_detail")
@XmlRootElement
public class PreEnrolDetail implements Serializable{
    @Id    
    @Column(name = "employee_id")
    private String employeeId;
    
    
    @Column(name = "first_name")
    private String firstName;
    
    
    @Column(name = "surname")
    private String surname;
    
    
    @Column(name = "middle_name")
    private String middleName;
    
        
    @Temporal(TemporalType.DATE) 
    @Column(name = "date_of_birth")
    private Date dateOfBirth;    
    
    
    @Column(name = "person_type")
    private int personType;
    
    
    @Column(name = "staff_type")
    private int staffType;
    
    
    @Column(name = "mda")
    private int mda;   
    
        
    @Temporal(TemporalType.DATE)    
    @Column(name = "date_of_employment")
    private Date dateOfEmployment;
        
    
    @Column(name = "salary_structure")
    private int salaryStructure;
        
    
    @Column(name = "grade_level")
    private int gradeLevel;
    
    
    @Column(name = "step")
    private int step;
    
    
    @Column(name = "generated_form_id")
    private String generatedFormId;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getPersonType() {
        return personType;
    }

    public void setPersonType(int personType) {
        this.personType = personType;
    }

    public int getStaffType() {
        return staffType;
    }

    public void setStaffType(int staffType) {
        this.staffType = staffType;
    }

    public Date getDateOfEmployment() {
        return dateOfEmployment;
    }

    public void setDateOfEmployment(Date dateOfEmployment) {
        this.dateOfEmployment = dateOfEmployment;
    }

    public int getGradeLevel() {
        return gradeLevel;
    }

    public void setGradeLevel(int gradeLevel) {
        this.gradeLevel = gradeLevel;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }    

    public String getGeneratedFormId() {
        return generatedFormId;
    }

    public void setGeneratedFormId(String generatedFormId) {
        this.generatedFormId = generatedFormId;
    }          

    public int getMda() {
        return mda;
    }

    public void setMda(int mda) {
        this.mda = mda;
    }

    public int getSalaryStructure() {
        return salaryStructure;
    }

    public void setSalaryStructure(int salaryStructure) {
        this.salaryStructure = salaryStructure;
    }
    
    
    
}
