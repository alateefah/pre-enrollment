/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.pojo;

import java.io.Serializable;

/**
 *
 * @author Lateefah
 */
public class AppMdaLookup implements Serializable{
        
    private int MdaId;
    private String description;
       
    public AppMdaLookup() {}

    public int getMdaId() {
        return MdaId;
    }

    public void setMdaId(int MdaId) {
        this.MdaId = MdaId;
    }
        
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
