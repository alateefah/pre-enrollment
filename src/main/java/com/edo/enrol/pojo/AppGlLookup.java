/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.pojo;

import java.io.Serializable;

/**
 *
 * @author Lateefah
 */
public class AppGlLookup implements Serializable{
        
    private int glId;
    private String description;
       
    public AppGlLookup() {}

    public int getGlId() {
        return glId;
    }

    public void setGlId(int glId) {
        this.glId = glId;
    }     
        
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
