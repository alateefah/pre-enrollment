/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.pojo;

import java.io.Serializable;

/**
 *
 * @author Lateefah
 */
public class AppStepLookup implements Serializable{
        
    private int stepId;
    private String description;
       
    public AppStepLookup() {}

    public int getStepId() {
        return stepId;
    }

    public void setStepId(int stepId) {
        this.stepId = stepId;
    }
        
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
