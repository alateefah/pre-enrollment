/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.pojo;

import java.io.Serializable;

/**
 *
 * @author Lateefah
 */
public class AppPersonTypeLookup implements Serializable{
        
    private int personTypeId;
    private String description;
       
    public AppPersonTypeLookup() {}

    public int getPersonTypeId() {
        return personTypeId;
    }

    public void setPersonTypeId(int personTypeId) {
        this.personTypeId = personTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
