/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.pojo;

import java.io.Serializable;

/**
 *
 * @author Lateefah
 */
public class AppSalaryStructureLookup implements Serializable{
        
    private int salaryStructureId;
    private String description;
       
    public AppSalaryStructureLookup() {}

    public int getSalaryStructureId() {
        return salaryStructureId;
    }

    public void setSalaryStructureId(int salaryStructureId) {
        this.salaryStructureId = salaryStructureId;
    }   
        
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
