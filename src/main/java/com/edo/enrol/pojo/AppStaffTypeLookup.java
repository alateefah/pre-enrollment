/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.pojo;

import java.io.Serializable;

/**
 *
 * @author Lateefah
 */
public class AppStaffTypeLookup implements Serializable{
        
    private int staffTypeId;
    private String description;
       
    public AppStaffTypeLookup() {}

    public int getStaffTypeId() {
        return staffTypeId;
    }

    public void setStaffTypeId(int staffTypeId) {
        this.staffTypeId = staffTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
