/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.pojo;

import java.util.Date;

/**
 *
 * @author Lateefah
 */
public class AppPreEnrolDetail {

    private String employeeId;
    private String firstName;
    private String surname;
    private String middleName;
    private Date dateOfBirth;
    private int personType;
    private int staffType;
    private int mda;
    private Date dateOfEmployment;
    private int salaryStructure;
    private int gradeLevel;
    private int step;
    private String generatedFormId;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getPersonType() {
        return personType;
    }

    public void setPersonType(int personType) {
        this.personType = personType;
    }

    public int getStaffType() {
        return staffType;
    }

    public void setStaffType(int staffType) {
        this.staffType = staffType;
    }

    public Date getDateOfEmployment() {
        return dateOfEmployment;
    }

    public void setDateOfEmployment(Date dateOfEmployment) {
        this.dateOfEmployment = dateOfEmployment;
    }

    public int getGradeLevel() {
        return gradeLevel;
    }

    public void setGradeLevel(int gradeLevel) {
        this.gradeLevel = gradeLevel;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }    
    
    public String getGeneratedFormId() {
        return generatedFormId;
    }

    public void setGeneratedFormId(String generatedFormId) {
        this.generatedFormId = generatedFormId;
    }

    public int getMda() {
        return mda;
    }

    public void setMda(int mda) {
        this.mda = mda;
    }

    public int getSalaryStructure() {
        return salaryStructure;
    }

    public void setSalaryStructure(int salaryStructure) {
        this.salaryStructure = salaryStructure;
    }

        
    
}
