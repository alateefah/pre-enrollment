/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.service;

import com.edo.enrol.manager.UserLoginManagerLocal;
import com.edo.enrol.manager.UserManagerLocal;
import com.edo.enrol.pojo.AppBoolean;
import com.edo.enrol.pojo.AppUser;
import com.edo.enrol.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Lateefah
 */
@Stateless
@Path("/v1/user")
public class UserService {
    @Context
    HttpServletRequest request;   
    
    @EJB    
    UserManagerLocal userManager;
    
    @EJB    
    UserLoginManagerLocal userLoginManager;
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUser(@QueryParam("username") String username,
                            @QueryParam("first-name") String firstName,
                            @QueryParam("other-names") String otherNames,
                            @QueryParam("password") String password) throws GeneralAppException {  
        
        AppUser appUser = userManager.addUser(username, password, firstName, otherNames);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/authenticate")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticateUser(@QueryParam("username") String username,
                            @QueryParam("password") String password) throws GeneralAppException {  
        
        AppUser appUser = userLoginManager.authenticateUser(username, password);
        return Response.ok(appUser).build();
           
    }

    @Path("/logout")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response logoutUser(@QueryParam("username") String username,
                            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppBoolean appBoolean = userLoginManager.logoutUser(username, rawToken);
        return Response.ok(appBoolean).build();
           
    }
    
    @Path("/change-password")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response changePassword(@QueryParam("username") String username,
                            @QueryParam("old-password") String oldPassword,
                            @QueryParam("new-password") String newPassword,
                            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppBoolean appBoolean = userManager.changeUserPassword(username, oldPassword, newPassword);
        return Response.ok(appBoolean).build();
           
    }
}
