/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.service;

import com.edo.enrol.manager.PreEnrolDetailManagerLocal;
import com.edo.enrol.manager.UserManagerLocal;
import com.edo.enrol.pojo.AppPreEnrolDetail;
import com.edo.enrol.pojo.AppUser;
import com.edo.enrol.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Lateefah
 */
@Stateless
@Path("/v1/pre-enrol")
public class PreEnrolDetailService {
    @Context
    HttpServletRequest request;   
    
    @EJB    
    PreEnrolDetailManagerLocal preEnrolDetailanager;
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addEmployee(@QueryParam("employee-id") String employeeId,
                            @QueryParam("first-name") String firstName,
                            @QueryParam("middle-name") String middleName,
                            @QueryParam("surname") String surname,
                            @QueryParam("dob") String datefBirth,
                            @QueryParam("person-type") String personType,
                            @QueryParam("staff-type") String staffType,
                            @QueryParam("mda") String mda,
                            @QueryParam("doe") String dateOfEmployment,
                            @QueryParam("salary-structure") String salaryStructure,
                            @QueryParam("grade-level") String gradeLevel,
                            @QueryParam("step") String step,
                            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppPreEnrolDetail appPreEnrolDetail = preEnrolDetailanager.addEmployee(employeeId, firstName, surname, middleName, 
                            datefBirth, personType, staffType, mda, dateOfEmployment, salaryStructure, gradeLevel, step, rawToken);
        
        return Response.ok(appPreEnrolDetail).build();
           
    }
    
   
}
