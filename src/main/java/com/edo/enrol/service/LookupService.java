/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edo.enrol.service;

import com.edo.enrol.manager.GlLookupManagerLocal;
import com.edo.enrol.manager.MdaLookupManagerLocal;
import com.edo.enrol.manager.PersonTypeLookupManagerLocal;
import com.edo.enrol.manager.SalaryStructureLookupManagerLocal;
import com.edo.enrol.manager.StaffTypeLookupManagerLocal;
import com.edo.enrol.manager.StepLookupManagerLocal;
import com.edo.enrol.pojo.AppGlLookup;
import com.edo.enrol.pojo.AppMdaLookup;
import com.edo.enrol.pojo.AppPersonTypeLookup;
import com.edo.enrol.pojo.AppSalaryStructureLookup;
import com.edo.enrol.pojo.AppStaffTypeLookup;
import com.edo.enrol.pojo.AppStepLookup;
import com.edo.enrol.util.exception.GeneralAppException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Lateefah
 */
@Stateless
@Path("/v1/lookup")
public class LookupService {
    @Context
    HttpServletRequest request;   
    
    @EJB    
    PersonTypeLookupManagerLocal personTypeLookupManager;
    
    @EJB    
    StaffTypeLookupManagerLocal staffTypeLookupManager;
    
    @EJB    
    GlLookupManagerLocal glLookupManager;
    
    @EJB    
    MdaLookupManagerLocal mdaLookupManager;
    
    @EJB    
    SalaryStructureLookupManagerLocal salaryStructureLookupManager;
    
    @EJB    
    StepLookupManagerLocal stepLookupManager;
    
    @GET
    @Path("/person-type")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPersonTypesLookup(@HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        List<AppPersonTypeLookup> appPersonTypeLookup = personTypeLookupManager.getAll(rawToken);
        return Response.ok(new GenericEntity <List<AppPersonTypeLookup>>(appPersonTypeLookup){}).build();
           
    }
    
    @GET
    @Path("/staff-type")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStaffTypesLookup(@HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        List<AppStaffTypeLookup> appStaffTypeLookup = staffTypeLookupManager.getAll(rawToken);
        return Response.ok(new GenericEntity <List<AppStaffTypeLookup>>(appStaffTypeLookup){}).build();
           
    }
    
    @GET
    @Path("/gl")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGlsLookup(@HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        List<AppGlLookup> appGlLookup = glLookupManager.getAll(rawToken);
        return Response.ok(new GenericEntity <List<AppGlLookup>>(appGlLookup){}).build();
           
    }
    
    @GET
    @Path("/mda")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMdasLookup(@HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        List<AppMdaLookup> appMdaLookup = mdaLookupManager.getAll(rawToken);
        return Response.ok(new GenericEntity <List<AppMdaLookup>>(appMdaLookup){}).build();
           
    }
    
    @GET
    @Path("/salary-structure")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSalaryStructureLookup(@HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        List<AppSalaryStructureLookup> appSalaryStructureLookup = salaryStructureLookupManager.getAll(rawToken);
        return Response.ok(new GenericEntity <List<AppSalaryStructureLookup>>(appSalaryStructureLookup){}).build();
           
    }
    
    @GET
    @Path("/step")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStepLookup(@HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        List<AppStepLookup> appStepLookup = stepLookupManager.getAll(rawToken);
        return Response.ok(new GenericEntity <List<AppStepLookup>>(appStepLookup){}).build();
           
    }
    
        
}
