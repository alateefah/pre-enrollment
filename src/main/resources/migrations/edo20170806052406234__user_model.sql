/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Lateefah
 * Created: Aug 4, 2017
 */

CREATE TABLE user(
    username VARCHAR(64) NOT NULL,
    firstname VARCHAR(50) NOT NULL,
    othernames VARCHAR(50) NOT NULL,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY (username)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE user_login (
    connection_id INT AUTO_INCREMENT NOT NULL,
    username VARCHAR(64) NOT NULL,
    remote_host VARCHAR(255) NOT NULL,
    login_time TIMESTAMP NOT NULL,
    logout_time DATETIME DEFAULT NULL,
    PRIMARY KEY(connection_id),
    CONSTRAINT fk_userlogin_username FOREIGN KEY (username) REFERENCES user (username) ON DELETE NO ACTION
)ENGINE=INNODB DEFAULT CHARSET=utf8;
