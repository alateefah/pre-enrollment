/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Lateefah
 * Created: Aug 13, 2017
 */

set foreign_key_checks = 1;
drop table if exists pre_enrol_detail;

drop table if exists gl;
CREATE TABLE gl (
  gl_id int NOT NULL auto_increment,
  description varchar(100) NOT NULL,
  PRIMARY KEY (gl_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO gl (description) VALUES
('Grade Level 1'),('Grade Level 2'),('Grade Level 3'),('Grade Level 5'),('Grade Level 4'),
('Grade Level 6'),('Grade Level 7'),('Grade Level 8'),('Grade Level 9'),('Grade Level 10'),
('Grade Level 11'),('Grade Level 12'),('Grade Level 13'),('Grade Level 14'),
('Grade Level 15'),('Grade Level 16'),('Grade Level 17');

create table pre_enrol_detail (
    employee_id varchar(64) not null,
    first_name varchar(64) not null,
    surname varchar(64) not null,
    middle_name varchar(64),
    date_of_birth date not null,
    person_type int not null,
    staff_type int not null,
    mda int not null,
    date_of_employment date not null,
    salary_structure int not null,
    grade_level int not null,
    step int not null,
    generated_form_id varchar(65),
    primary key (employee_id),
    constraint foreign key (person_type) references person_type(person_type_id),
    constraint foreign key (staff_type) references staff_type(staff_type_id),
    constraint foreign key (mda) references mda(mda_id),
    constraint foreign key (salary_structure) references salary_structure(salary_structure_id),
    constraint foreign key (grade_level) references gl(gl_id),
    constraint foreign key (step) references step(step_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;