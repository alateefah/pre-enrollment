/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Lateefah
 * Created: Aug 13, 2017
 */

drop table if exists step;
CREATE TABLE step (
  step_id int NOT NULL auto_increment,
  description varchar(100) NOT NULL,
  PRIMARY KEY (step_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO step (description) VALUES
('Step 1'),('Step 2'),('Step 3'),('Step 5'),('Step 4'),('Step 6'),('Step 7'),('Step 8'),
('Step 9'),('Step 10'),('Step 11'),('Step 12'),('Step 13'),('Step 14'),('Step 15'),('Step 16'),
('Step 17');