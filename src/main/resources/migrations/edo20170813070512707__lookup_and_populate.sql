/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Lateefah
 * Created: Aug 12, 2017
 */

drop table if exists salary_structure;
create table salary_structure (
    salary_structure_id int auto_increment,
    description VARCHAR(20) not null,
    primary key (salary_structure_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;


INSERT INTO salary_structure (description) VALUES
('ASCE'),('ASCE_11'),('ASCE_7'),('BPE07'),('BPE11'),('BPP10'),('BPP11'),('CONHESS'),
('CONMESS'),('CONPCASS'),('CONPM'),('CONPOSS'),('CONPSS'),('CONRAISS'),('CONRD10'),
('CONTEDISS'),('CONTISS'),('CONUASS'),('DMO'),('DMO_09'),('DMO_11'),('ENCOMPM10'),
('ENCONPOSS10'),('ENCONPSS'),('ENCONTISS'),('ENCUASS'),('FIRS'),('HAPOSS'),('HAPSS'),
('HATISS'),('ICPC/EFCC07'),('ICPC/EFCC10'),('ICPC/EFCC12'),('ICRC09'),('ICRC15'),
('N/A'),('NAICOM'),('NAPTIN'),('NCDMB11'),('NCDMB13'),('NCSAT'),('NCSAT2'),('NEITI'),
('NELEC11'),('NEPC07'),('NEPC15'),('NEPZA'),('NERC'),('NHIS11'),('NHIS14'),('NIC09'),
('NIMET'),('NIMET12'),('NIPC'),('NIPC2'),('NIPC3'),('OGFZA12'),('OGFZA14'),('PERM.SEC'),
('PM'),('PPPRA'),('PPPRA10'),('PPPRA13'),('PPPRA_7'),('PRA07'),('PTAD13'),('PTI04'),
('PTI07'),('PTI11'),('PTI13'),('PTI15'),('REA'),('TOPSAL1'),('TOPSAL2'),('TOPSAL3'),('TOPSAL4'),
('TOPSAL4 (10)'),('UASS');


drop table if exists gl;
CREATE TABLE gl (
  gl_id varchar(10) NOT NULL,
  description varchar(100) NOT NULL,
  PRIMARY KEY (gl_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO gl (gl_id, description) VALUES
('00', '00'),('1', '01'),('10', '10'),('11', '11'),('11A', '11A'),('11B', '11B'),
('12', '12'),('13', '13'),('14', '14'),('15', '15'),('16', '16'),('17', '17'),
('18', '18'),('19', '19'),('2', '02'),('3', '03'),('4', '04'),('5', '05'),('6', '06'),
('7', '07'),('8', '08'),('9', '09'),('9A', '9A'),('9B', '9B'),('A1', 'A1'),('A2', 'A2'),
('A3', 'A3'),('A4', 'A4'),('B1', 'B1'),('B2', 'B2'),('B3', 'B3'),('C1', 'C1'),('C2', 'C2'),
('C3', 'C3'),('C4', 'C4'),('C5', 'C5'),('CONS', 'CONS'),('D1', 'D1'),('D2', 'D2'),('D3', 'D3'),
('D4', 'D4'),('D5', 'D5'),('EG1', 'EG1'),('EG2', 'EG2'),('EG3', 'EG3'),('J01', 'J01'),('J02', 'J02'),
('J03', 'J03'),('J04', 'J04'),('JS1', 'JS1'),('JS2', 'JS2'),('JS3', 'JS3'),('JS4', 'JS4'),('JS5', 'JS5'),
('JSI', 'JSI'),('M04', 'M04'),('M05', 'M05'),('M06', 'M06'),('M1', 'M1'),('M2', 'M2'),('M3', 'M3'),('M4', 'M4'),
('M5', 'M5'),('M6', 'M6'),('MM1', 'MM1'),('MM3', 'MM3'),('S01', 'S01'),('S02', 'S02'),('S03', 'S03'),('S04', 'S04'),
('S05', 'S05'),('S06', 'S06'),('S07', 'S07'),('SM1', 'SM1'),('SM2', 'SM2'),('SM3', 'SM3'),('SS1', 'SS1'),('SS2', 'SS2'),
('SS3', 'SS3'),('SS4', 'SS4'),('SS5', 'SS5'),('SS6', 'SS6'),('SS7', 'SS7');

drop table if exists mda;
create table mda (
    mda_id int auto_increment not null,
    description varchar(150) not null,
    PRIMARY KEY (mda_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into mda (description) values
('Agricultural Development Program'), ('Ambrose Alli University'), ('Bendel Nwspaper Limited');

drop table if exists pre_enrol_detail;
create table pre_enrol_detail (
    employee_id varchar(64) not null,
    first_name varchar(64) not null,
    surname varchar(64) not null,
    middle_name varchar(64),
    date_of_birth date not null,
    person_type int not null,
    staff_type int not null,
    mda int not null,
    date_of_employment date not null,
    salary_structure int not null,
    grade_level varchar(10) not null,
    step varchar(100) not null,
    generated_form_id varchar(65),
    primary key (employee_id),
    constraint foreign key (person_type) references person_type(person_type_id),
    constraint foreign key (staff_type) references staff_type(staff_type_id),
    constraint foreign key (mda) references mda(mda_id),
    constraint foreign key (salary_structure) references salary_structure(salary_structure_id),
    constraint foreign key (grade_level) references gl(gl_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;