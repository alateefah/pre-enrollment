/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Lateefah
 * Created: Aug 6, 2017
 */

drop table if exists person_type;
create table person_type (
    person_type_id int not null auto_increment,
    description varchar(64) not null,
    primary key (person_type_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

insert into person_type (description) values 
            ('State Staff'),
            ('State Pensioner'), 
            ('LGA Staff'),
            ('SUBEB'),
            ('LGA Pensioner'),
            ('SUBEB Pensioner');

drop table if exists staff_type;
create table staff_type (
    staff_type_id int not null auto_increment,
    description varchar(64) not null,
    primary key (staff_type_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

insert into staff_type (description) values 
    ('Political Appointment'),('Permanent'),('Contract'), ('Intern');

drop table if exists pre_enrol_detail;
create table pre_enrol_detail (
    employee_id varchar(64) not null,
    first_name varchar(64) not null,
    surname varchar(64) not null,
    middle_name varchar(64),
    date_of_birth date not null,
    person_type int not null,
    staff_type int not null,
    mda varchar(255) not null,
    date_of_employment date not null,
    salary_structure varchar(100) not null,
    grade_level varchar(100) not null,
    step varchar(100) not null,
    generated_form_id varchar(65),
    primary key (employee_id),
    constraint foreign key (person_type) references person_type(person_type_id),
    constraint foreign key (staff_type) references staff_type(staff_type_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;