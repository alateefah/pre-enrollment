/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
 
var gulp = require('gulp');
var angular2 = require('gulp-angular2');
 
gulp.task('watch', function() {
  gulp.watch('./src/app/**/*.ts', ['angular2app']);
  gulp.watch('./src/app/**/*.html', ['angular2app']);
  gulp.watch('./src/app/**/*.css', ['angular2app']);
});

gulp.task('angular2app', function () {
  return gulp.src('./src/app/main.js')
    .pipe(angular2())
    .pipe(gulp.dest('./public/app'));
});
