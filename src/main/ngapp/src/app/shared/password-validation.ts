import {AbstractControl} from '@angular/forms';


export class PasswordValidation {

    static MatchPassword(AC: AbstractControl) {
       let password = AC.get('new-password').value; // to get value in input tag
       let confirmPassword = AC.get('confirm-new-password').value; // to get value in input tag
        if(password != confirmPassword) {
            AC.get('confirm-new-password').setErrors( {MatchPassword: true} )
        } else {
            return null
        }
    }
}