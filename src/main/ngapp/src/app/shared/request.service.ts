import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
declare var jQuery: any;

@Injectable()
export class RequestService {
  
  constructor(private http: Http,
              private router: Router) { }

  headers = new Headers({ 'Content-Type': 'application/json' });   
  options = new RequestOptions({ headers:this.headers }); 
  currentUser = JSON.parse(localStorage.getItem('currentUser'));
  base_url = window.location.protocol+'//'+window.location.host+"/edo/api/v1/";
  logoutUrl: string = "user/logout";
  
  get = (url, formData, auth): Observable<any> => {
    if (auth && !this.headers.has("Authorization")) {
      this.headers.append("Authorization", "Bearer "+this.currentUser.authToken);
    }
    return this.http.get(this.base_url+url+'?'+jQuery.param(formData), this.options) 
                         .map((res:Response) => res.json()) 
                         .catch((error:any) => {                                         
                            if (error.json()) {
                              if (error.json().message === "Invalid token") {
                                this.logout().subscribe();
                              }
                            }          
                            return Observable.throw(error.json() || 'Server error'); 
                         })
  }

  post = (url, formData, auth): Observable<any> => {
    if (auth && !this.headers.has("Authorization")) {
      this.headers.append("Authorization", "Bearer "+this.currentUser.authToken);
    }
    return this.http.post(this.base_url+url+'?'+jQuery.param(formData), {}, this.options) 
              .map((res:Response) => res.json()) 
              .catch((error:any) => {                                
                if (error.json()) {
                  if (error.json().message === "Invalid token") {
                    this.logout().subscribe();
                  }
                }          
                return Observable.throw(error.json() || 'Server error'); 
              })
  }

  logout(): Observable<any> {    
    localStorage.removeItem("currentUser");
    this.router.navigate(['/login']) 
    return this.post(this.logoutUrl, {'username':this.currentUser.username}, true);
    
  }

}
