import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { RequestService } from '../shared/request.service';
declare var jQuery: any;

@Injectable()
export class ChangePasswordService {

  changePasswordUrl: string = "user/change-password";

  constructor(private http: Http,
                private request: RequestService) { }

  changePassword(formData): Observable<any[]>  {
        console.log(formData);
        jQuery.extend(true, formData,{username: JSON.parse(localStorage.getItem("currentUser")).username});
        console.log(formData);
        return this.request.post(this.changePasswordUrl, formData, true);
    }

}
