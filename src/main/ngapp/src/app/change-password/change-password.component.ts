import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { ChangePasswordService } from './change-password.service';
import { RequestService } from '../shared/request.service';
import { PasswordValidation } from '../shared/password-validation';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css'],
  providers: [ ChangePasswordService ]
})
export class ChangePasswordComponent implements OnInit {

  changePasswordForm : FormGroup;
  formError: String = null;
  formSuccess: String = null;

  constructor(private fb: FormBuilder,
              private _changePasswordService: ChangePasswordService) { 
    this.createChangePasswordForm();
  }

  ngOnInit() {
  }

   createChangePasswordForm () {
    this.changePasswordForm = this.fb.group(
      {
        'old-password': ['', Validators.required],
        'new-password': ['', Validators.required],
        'confirm-new-password': ['', Validators.required]
      }, 
      {
        validator: PasswordValidation.MatchPassword // your validation method
      }
    )
  };

  changePassword() {
    if (this.changePasswordForm.valid) {          
      this._changePasswordService.changePassword(this.changePasswordForm.value) 
        .subscribe(
            response => {
              this.formSuccess =  "Password Changed successful";
              this.changePasswordForm.reset();
              setTimeout(() => {
                   this.formSuccess = null;
              }, 7000);             
            }, 
            err => {
                this.changePasswordForm.reset();
                this.formError =  err.developerMessage;
            });      
    } else {
      for (let inner in this.changePasswordForm.controls) {
        this.changePasswordForm.get(inner).markAsTouched();
      }
      this.formError = "Invalid Form. Fill in the highlighted field";
      return;
    }
  }
}
