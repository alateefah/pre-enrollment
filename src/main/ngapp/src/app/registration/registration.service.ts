import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
declare var jQuery: any;
import { RequestService } from '../shared/request.service';

@Injectable()
export class RegistrationService {
    authenticatUrl: string = "pre-enrol";
    personTypesUrl: string = "lookup/person-type";
    staffTypesUrl: string = "lookup/staff-type";
    gradeLevelUrl: string = "lookup/gl";
    mdaUrl: string = "lookup/mda";
    salaryStructureUrl: string = "lookup/salary-structure";
    stepsUrl: string = "lookup/step";

    constructor(private http: Http,
                private request: RequestService) { }


    enrol(formData): Observable<any[]>  {
        return this.request.post(this.authenticatUrl, formData, true);
    }

    getPersonTypes():  Observable<any[]>  {
        return this.request.get(this.personTypesUrl, {}, true);
    }

    getStaffTypes():  Observable<any[]>  {
        return this.request.get(this.staffTypesUrl, {}, true);
    }

    getGradeLevels():  Observable<any[]>  {
        return this.request.get(this.gradeLevelUrl, {}, true);
    }

    getMDAs():  Observable<any[]>  {
        return this.request.get(this.mdaUrl, {}, true);
    }

    getSalaryStructure():  Observable<any[]>  {
        return this.request.get(this.salaryStructureUrl, {}, true);
    }

    getSteps():  Observable<any[]>  {
        return this.request.get(this.stepsUrl, {}, true);
    }


}
