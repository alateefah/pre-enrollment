import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { RegistrationService } from './registration.service';
import { RequestService } from '../shared/request.service';

import * as $ from 'jquery';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers: [RegistrationService, RequestService]
})
export class RegistrationComponent implements OnInit {

  registrationForm: FormGroup;
  formError: String = null;
  personTypes: any = null;
  staffTypes: any = null;
  gradeLevels: any = null;
  mdas: any = null;
  salaryStructures: any = null;
  steps: any = null;
  successfulEnrollment: boolean = false;
  enrolee: any = null;

  constructor(private fb: FormBuilder,
    private _registrationService: RegistrationService) {
    this.createRegistrationForm();
    this.registrationForm.statusChanges.subscribe(data => {
      if (data === 'VALID') {
        this.formError = null;
      }
    })
  }

  ngOnInit() {
    var script = document.createElement('script');
    script.innerText = "$('select.dropdown').dropdown();";
    $('body').prepend(script);

      this._registrationService.getPersonTypes()
        .subscribe(
        response => {
          this.personTypes = response;
        },
        err => {
          console.log(err);
          this.formError = err.developerMessage;
      });

      this._registrationService.getStaffTypes()
        .subscribe(
        response => {
          this.staffTypes = response;
        },
        err => {
          console.log(err);
          this.formError = err.developerMessage;
      });

      this._registrationService.getGradeLevels()
        .subscribe(
        response => {
          this.gradeLevels = response;
        },
        err => {
          // Log errors if any
          console.log(err);
          this.formError = err.developerMessage;
      });
      
      this._registrationService.getMDAs()
        .subscribe(
        response => {
          this.mdas = response;
        },
        err => {
          console.log(err);
          this.formError = err.developerMessage;
      });

      this._registrationService.getSalaryStructure()
        .subscribe(
        response => {
          this.salaryStructures = response;
        },
        err => {
          console.log(err);
          this.formError = err.developerMessage;
      });

      this._registrationService.getSteps()
        .subscribe(
        response => {
          this.steps = response;
        },
        err => {
          // Log errors if any
          console.log(err);
          this.formError = err.developerMessage;
      });

  }

  createRegistrationForm() {
    this.registrationForm = this.fb.group({
      'employee-id': ['', Validators.required],
      'first-name': ['', Validators.required],
      'middle-name': [''],
      'surname': ['', Validators.required],
      'dob': ['', Validators.required],
      'person-type': ['', Validators.required],
      'staff-type': ['', Validators.required],
      'doe': ['', Validators.required],
      'mda': ['', Validators.required],
      'salary-structure': ['', Validators.required],
      'grade-level': ['', Validators.required],
      'step': ['', Validators.required]
    })
  };

  enrolEmployee() {
    $("#registrationForm").addClass("loading");
    if (this.registrationForm.valid) {
      this._registrationService.enrol(this.registrationForm.value)
        .subscribe(
        response => {
          console.log(response);
          this.enrolee = response;
          this.registrationForm.reset();
          this.successfulEnrollment = true;
        },
        err => {
          // Log errors if any
          console.log(err);
          this.formError = err.developerMessage;
          $("#registrationForm").removeClass("loading");
        });


    } else {
      for (let inner in this.registrationForm.controls) {
        this.registrationForm.get(inner).markAsTouched();
        //$("#registrationForm").removeClass("loading");
      }
      this.formError = "Invalid Form. Fill in the highlighted field";
      $("#registrationForm").removeClass("loading");
      return;
    }
  }

  goBack() {
    this.successfulEnrollment = false;
    this.enrolee = null;
    $("#registrationForm").removeClass("loading");
  }

}
