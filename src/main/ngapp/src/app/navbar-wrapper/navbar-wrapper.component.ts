import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RequestService } from '../shared/request.service';

@Component({
  selector: 'app-navbar-wrapper',
  templateUrl: './navbar-wrapper.component.html',
  styleUrls: ['./navbar-wrapper.component.css'],
  providers: [RequestService]
})
export class NavbarWrapperComponent implements OnInit {
  title: String;
  user : any;
  
  constructor(private route: ActivatedRoute,
              private _requestService: RequestService) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("currentUser"));
  }

  logout() {
    this._requestService.logout().subscribe(
            response => {
              console.log(response);                
            }, 
            err => {
                console.log(err);
                //this.formError =  err.developerMessage;
            });
  }

  
}
