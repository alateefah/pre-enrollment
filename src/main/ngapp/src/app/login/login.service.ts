import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
declare var jQuery: any;
import { RequestService } from '../shared/request.service';

@Injectable()
export class LoginService {
    authenticatUrl: string = "user/authenticate";
    
    constructor(private http: Http,
                private request: RequestService) { }


    authenticate(formData): Observable<any[]>  {
        return this.request.post(this.authenticatUrl, formData, false);
    }


}
