import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { LoginService } from './login.service';
import { RequestService } from '../shared/request.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import * as $ from 'jquery';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService, RequestService]
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup;
  formError: String = null;

  constructor(private fb: FormBuilder,
              private _loginService: LoginService,
              private router: Router) {
    this.createLoginForm();
    this.loginForm.statusChanges.subscribe(data => {
      if (data === 'VALID') {
        this.formError = null;
      }
    })
  }


  createLoginForm () {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  };


  ngOnInit() {
    localStorage.clear();
  }

  loginUser() {
    $("#loginForm").addClass("loading");
    if (this.loginForm.valid) {
      this._loginService.authenticate(this.loginForm.value)
        .subscribe(
            response => {
              localStorage.setItem('currentUser', JSON.stringify(response));
              $("#loginForm").removeClass("loading");
              this.router.navigate(['/register']);
            },
            err => {
                this.formError =  err.developerMessage;
                $("#loginForm").removeClass("loading");
            });
    } else {
      for (let inner in this.loginForm.controls) {
        this.loginForm.get(inner).markAsTouched();
      }
      this.loginForm.markAsTouched();
      this.formError = "Invalid Form. Fill in the highlighted field";
      $("#loginForm").removeClass("loading");
      return;
    }
  }
}
