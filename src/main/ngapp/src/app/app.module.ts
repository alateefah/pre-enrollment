import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms'; 
import { RouterModule, Routes } from '@angular/router';
import {APP_BASE_HREF } from '@angular/common';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { AuthGuard } from './auth-guard.service';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { NavbarWrapperComponent } from './navbar-wrapper/navbar-wrapper.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/register', pathMatch: 'full' },
  { path: 'login',  component: LoginComponent  }, 
  {
    path: '', 
    component: NavbarWrapperComponent, 
    canActivate: [AuthGuard],
    children: [
      { path: 'register', component: RegistrationComponent },
      { path: 'change-password', component: ChangePasswordComponent}
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    ChangePasswordComponent,
    NavbarWrapperComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    {provide: APP_BASE_HREF, useValue: '/edo'}, 
    AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
